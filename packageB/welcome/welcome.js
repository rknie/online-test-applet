const app = getApp();
const amapFile = require('../../utils/amap-wx.130');
Page({
  data: {
    surname:'',
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    ColorList: app.globalData.ColorList,
    isLoading: false,
    spinShow: false,
    recruits: [{
        title: '立即进入',
        introduce: '立即进入在线考试系统！',
        bgColor: 'bg-blue',
        bgImgUrl: 'http://img.uelink.com.cn/upload/xykj/bg_eval.png',
        url: 'eval/index'
      }
    ]
  },
  onLoad: function () {

  },
    /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },
  gotoIndex() {
    wx.navigateTo({
      url: '../index/index',
    })
  },

})