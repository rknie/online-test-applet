
const app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
		name: '',
		pwd: '',
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
	
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	url: function () {
		wx.reLaunch({
			url: '/pages/index/index',
		})
	},

	bindGetPhoneNumber: async function (e) {
		if (this.data.name.length < 6 || this.data.name.length > 30) {
			wx.showToast({
				title: '账号输入错误(6-30位)',
				icon: 'none'
			});
			return;
		}

		if (this.data.pwd.length < 6 || this.data.pwd.length > 30) {
			wx.showToast({
				title: '密码输入错误(6-30位)',
				icon: 'none'
			});
			return;
		}

		let params = {
			name: this.data.name,
			pwd: this.data.pwd
		}; 
		let opt = {
			title: '登录中'
		};
		wx.navigateTo({
			url: '/packageE/index/home',
		})
		// wx.showModal({
		// 	title: "请使用管理员绑定的微信进行登录！"
		// })
	
	}

})