const app = getApp()
Page({
    data: {
      StatusBar: app.globalData.StatusBar,
      CustomBar: app.globalData.CustomBar,
      Custom: app.globalData.Custom,
      list: [{
          name: '武汉分公司'
        },
        {
          name: '长沙分公司'
        },
        {
          name: '江西分公司'
        },
        {
          name: '合肥分公司'
        }
      ],
      current: 0,
    },
    onLoad(options) {
      let _this = this;

    },
    change: function (e) {
     const id= e.currentTarget.dataset.index;
      console.log(id)
      this.setData({
        current:id
      })
     
    },
    kaipiao: function () {
      wx.setClipboardData({
        data: "单位名称:上海威派格智慧水务股份有限公司武汉分公司   纳税人识别号:914201063036362849     地址及电话:武汉东湖新技术开发区关山大道21号泛悦城T2写字楼17层01-09号写字间（自贸区武汉片区）027-87778906     开户行及账号:交通银行武汉水果湖支行 421869419018800053065",
        success(res) {
          wx.getClipboardData({
            success(res) {
              console.log(res.data) // data
              wx.showToast({
                title: '复制成功,粘贴到记事本',
                icon: 'none'
              })
            }
          })
        }
      })
    }
  }

)