const app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
		bottom: 100,
    bgColor2: "#5677fc",
    btnList: [{
      bgColor: "#16C2C2",
      //名称
      text: "搜索",
      //字体大小
      fontSize: 28,
      //字体颜色
      color: "#fff"
    }, {
      bgColor: "#64B532",
      //名称
      text: "关于",
      //字体大小
      fontSize: 28,
      //字体颜色
      color: "#fff"
    }, {
      bgColor: "#FFA000",
      //名称
      text: "通知",
      //字体大小
      fontSize: 28,
      //字体颜色
      color: "#fff"
    }],
		mode: 'circle', // circle 圆形 square 方形
		bottom: 100, //距离顶部距离 rpx
		top: 50, //距离顶部多少距离显示 px
		bgColor: 'blue', //背景色
		color: '#fff', //文字图标颜色
		icon: 'triangleupfill', //图标
		right: '24', //距离右侧距离 rpx
		scrollTop: 0,
		StatusBar: app.globalData.StatusBar,
		CustomBar: app.globalData.CustomBar,
		spinShow: false,
		swiperList: [{
			id: 0,
			type: 'image',
			url: 'https://www.dabenben.cn:9001/dma/logo.jpg'
		}, {
			id: 1,
			type: 'image',
			url: 'https://light-year-1258515630.cos.ap-guangzhou.myqcloud.com/%E5%B0%8F%E9%A2%98%E5%BA%93Lite/2.png',
		}],
		userInfo:null
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: async function (options) {
	this.setData({
		userInfo:app.globalData.userInfo
	})
	wx.pageScrollTo({
		scrollTop: 1600,
		duration: 4000,
	})
	setTimeout(function () {
		wx.pageScrollTo({
			scrollTop: 0,
			duration: 300,
		})
	}, 4000);
	},
	url:function(e){
		const url=e.currentTarget.dataset.url
		console.log(url)
		if(url=="/packageC/friend/friend"){
			wx.getUserProfile({
        desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
        success: (res) => {
          console.log(res)
          wx.setStorageSync('userInfo', res.userInfo)
          wx.setStorageSync('nickName', res.userInfo.nickName)
          wx.setStorageSync('avatarUrl', res.userInfo.avatarUrl)
          wx.setStorageSync('hasUserInfo', true)
        }
			})
			wx.navigateTo({
				url: '/packageC/friend/friend',
			});
			return
		} 
		if(url=="pages/index/index"){
			wx.navigateToMiniProgram({
				appId: 'wx90532cf6d92591d9',
				path: 'pages/index/index',
				extraData: {
					foo: 'bar'
				},
				envVersion: 'develop',
				success(res) {
				}
			})
			return
		};
		if(url=="wait"){
			wx.showToast({
				title: '功能开发中！',
				icon: 'loading',
				duration: 2000
				})
			return
		};
		wx.navigateTo({
			url: url,
		})
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {},
	//页面滚动执行方式
	onPageScroll(e) {
		this.setData({
			scrollTop: e.scrollTop
		})
	},
	previewImage: function (e) {
		var current = e.target.dataset.src;
		wx.previewImage({
			current: current, // 当前显示图片的http链接  
			urls: this.data.cooperation_img // 需要预览的图片http链接列表  
		})
	},


	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},
	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})