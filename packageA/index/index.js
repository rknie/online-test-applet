//index.js
const app = getApp();
Page({
  data:{
    userInfo:app.globalData.userInfo
  },
onLoad:function(){
 this.setData({
  userInfo:app.globalData.userInfo
 })
},
  create: function(){
    wx.navigateTo({
      url: '/packageA/create/create',
    })
  },

  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '欢迎使用在线考试',
      path: '/packageA/index/index',
      success: function (res) {
        wx.showToast({
          title: '转发成功',
          icon: "none",
          duration: 1500
        })
      },
      fail: function (res) {
        wx.showToast({
          title: '转发失败',
          icon: "none",
          duration: 1500
        })
      }
    }
  }
})
