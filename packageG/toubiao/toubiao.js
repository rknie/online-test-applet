// pages/school/cert.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    total:0,
    opacity: '.3', //字体透明度
    color2: '#081EF', //字体颜色
    number: 6, //水印数量
    deg: '-45', //旋转角度
    mode: 'circle', // circle 圆形 square 方形
    bottom: 100, //距离顶部距离 rpx
    top: 50, //距离顶部多少距离显示 px
    bgColor: 'blue', //背景色
    color: '#fff', //文字图标颜色
    icon: 'triangleupfill', //图标
    right: '24', //距离右侧距离 rpx
    scrollTop: 0,
    isLoading: true,
    userInfo: null,
    modalName: null,
    neirong: null,
    orderby: '招标投标-招标文件',
    orderbyList: [{
      title: '招标文件',
      value: '招标投标-招标文件'
    }, {
      title: '投标文件',
      value: '招标投标-投标文件'
    }, {
      title: '业绩文件',
      value: '招标投标-业绩文件'
    }, {
      title: '资质文件',
      value: '招标投标-资质文件'
    }],
    queryParam: {
      pageIndex: "1",
      pageSize: "100",
      name: "招标投标-招标文件"
    },
    cardColor: ['red', 'orange', 'yellow', 'olive', 'green', 'blue', 'purple', 'mauve', 'pink'],
    certList: [],
    code: ""
  },
  keywordInput: function (e) {
    this.setData({
      'queryParam.title': e.detail.value
    });
  },
  searchBooks:function (params) {
    this.getHotBooksData(this.data.orderby)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  //页面滚动执行方式
  onPageScroll(e) {
    this.setData({
      scrollTop: e.scrollTop
    })
  },
  inital: function (orderby) {
    this.setData({
      'queryParam.name': orderby,
      userInfo: app.globalData.userInfo
    });
  },
  orderbyChanged: function (e) {
    const orderby = this.data.orderbyList[e.currentTarget.dataset.index].value;
    this.setData({
      orderby: orderby
    });
    this.getHotBooksData(orderby);
  },
  getHotBooksData: function (orderby) {
    this.inital(orderby)
    app.formPost('/api/wx/student/book/pageList', this.data.queryParam)
      .then(res => {
        if (res.code == 1) {
          this.setData({
            certList: res.response.list,
            total:res.response.total,
            isLoading: false
          })
        } else {
          wx.showModal({
            title: res.message
          })
        }
      }).catch(e => {
        wx.showModal({
          title: e
        })
      })
  },

  onLoad: function (options) {
    const orderby = options.orderby ? options.orderby : '招标投标-招标文件';
    this.verifycode = this.selectComponent("#verifycode");
    this.setData({
      userInfo: app.globalData.userInfo
    })
    this.inital(orderby);
    this.getHotBooksData(orderby);
    this.initall();
  },
  initall: function () {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  navActivity(e) {
    const id = e.currentTarget.dataset.id;
    const dept = e.currentTarget.dataset.dept;
    //弹出组件,此处必须把this重新赋予变量不然callback内部会提示找不到this
    var _this = this;
    _this.verifycode.showView({
      phone: "18942109494",
      inputSuccess: function (phoneCode) {
        //调用组件关闭方法
        if (dept == phoneCode) {
          _this.verifycode.closeView();
          wx.navigateTo({
            url: '../process/process?id=' + id,
          });
        } else {
          _this.verifycode.closeView();
          wx.showToast({
            title: '密码输入错误！',
            icon: 'error',
            duration: 2000
            })
        }
      }
    });



    //   wx.navigateTo({
    //     url: '../process/process?id=' + id,
    // });
  },
  goToHook(e) {
    var that = this;
    const url = e.currentTarget.dataset.target
    console.log(url)
    that.setData({
      modalName: "Modal",
      neirong: url
    });
    wx.setClipboardData({
      data: url,
      success(res) {
        wx.getClipboardData({
          success(res) {
            console.log(res.data) // data
            wx.showToast({
              title: '复制成功,粘贴到浏览器访问',
              icon: 'none'
            })
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    // setTimeout(function () {
    //   that.setData({
    //     isLoading: false
    //   });
    // }, 400);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '“在线考试” - 招标投标',
      path: '/packageG/toubiao/toubiao'
    }
  },
})