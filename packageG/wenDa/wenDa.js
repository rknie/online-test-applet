// pages/course/search.js
var app = getApp();

Page({
  data: {
    opacity: '.3', //字体透明度
    color2: '#081EF', //字体颜色
    number: 6, //水印数量
    deg: '-45', //旋转角度
    mode: 'circle', // circle 圆形 square 方形
    bottom: 100, //距离顶部距离 rpx
    top: 50, //距离顶部多少距离显示 px
    bgColor: 'blue', //背景色
    color: '#fff', //文字图标颜色
    icon: 'triangleupfill', //图标
    right: '24', //距离右侧距离 rpx
    scrollTop: 0,
    header: {
      defaultValue: '',
      inputValue: '',
      help_status: false,
      help_class_status: false
    },
    main: {
      mainDisplay: true, // main 显示的变化标识
      list: []
    },
    pageType: 'teacher',
    spinShow: false,
    tag: true,
    loadMoreLoad: false,
    loadMoreTip: '暂无数据',
    queryParam: {
      pageIndex: 1,
      pageSize: 10,
      title: '',
    },
    tableData: [],
    tableDataList: [],
    total: 1,
    shuLiang: 0,
    status:'',
    value:"",
  },
  loadmore(){
    setTimeout(() => {
      this.setData({
        'queryParam.pageSize':this.data.queryParam.pageSize+10,
        status:'loadmore'
      })
      app.formPost('/api/wx/student/wenda/pageList', this.data.queryParam)
      .then(res => {
        if (res.code === 1) {
          const re = res.response
          this.setData({
            ['queryParam.pageIndex']: re.pageNum,
            tableDataList: re.list,
            total: re.pages
          });
          if (re.pageNum >= re.pages) {
            this.setData({
              loadMoreLoad: false,
              loadMoreTip: '暂无数据'
            });
          }
        }
      })
    }, 500);
  },
    // 触底加载
    onReachBottom(){
      this.setData({
        status:'loading'
      })
      // 模拟加载
      if(this.data.tableData.length<7){
        this.loadmore()
      }else{
        this.setData({
          status:'nomore'
        })
      }
    },
  //页面滚动执行方式
  onPageScroll(e) {
    this.setData({
      scrollTop: e.scrollTop
    })
  },
  bindClearSearchTap: function (e) {
    this.setData({
      'main.mainDisplay': true,
      'main.total': 0,
      'queryParam.title': '',
      tag: true
    });
  },
  bindSearchInput: function (e) {
    if (this.data.main.mainDisplay != false) {
      this.setData({
        'main.mainDisplay': !this.data.main.mainDisplay
      });
    }
    this.setData({
      'queryParam.title': e.detail.value,
      value: e.detail.value
    });
    this.setData({
      queryParam: {
        pageIndex: 1,
        pageSize: app.globalData.pageSize,
        title: e.detail.value
      },
    })
    this.search();
    return e.detail.value;
  },

  bindSearchInputClass: function (e) {
    if (this.data.main.mainDisplay != false) {
      this.setData({
        'main.mainDisplay': !this.data.main.mainDisplay
      });
    }
    this.setData({
      'queryParam.title': e.detail.value
    });
    this.searchClass();
    return e.detail.value;
  },

  // 点击搜索教师
  bindConfirmSearchTap: function () {
    this.search();
  },
  // 点击搜索班级
  bindConfirmSearchTapClass: function () {
    this.searchClass();
  },
  // 搜索教师
  search: function (key) {
    if (this.data.queryParam.title.length < 1) {
      wx.showToast({
        title: '请输入问题名称',
        image: '../../images/info.png'
      });
      return;
    }
    let _this = this
    _this.setData({
      tableData: [],
      shuLiang: 0,
      tag: false
    });
    app.formPost('/api/wx/student/wenda/pageList', _this.data.queryParam)
      .then(res => {
        if (res.code === 1) {
          const re = res.response
          _this.setData({
            ['queryParam.pageIndex']: re.pageNum,
            tableData: key ? re.list : _this.data.tableData.concat(re.list),
            total: re.pages,
            shuLiang: re.total
          });
          if (re.pageNum >= re.pages) {
            _this.setData({
              loadMoreLoad: false,
              loadMoreTip: '暂无数据'
            });
          }
        }
      }).catch(e => {
        _this.setData({
          spinShow: false
        });
        app.message(e, 'error')
      })

  },
  onLoad: function (options) {
    let _this = this
    app.formPost('/api/wx/student/wenda/pageList', _this.data.queryParam)
      .then(res => {
        if (res.code === 1) {
          const re = res.response
          _this.setData({
            ['queryParam.pageIndex']: re.pageNum,
            tableDataList: re.list,
            total: re.pages
          });
          if (re.pageNum >= re.pages) {
            _this.setData({
              loadMoreLoad: false,
              loadMoreTip: '暂无数据'
            });
          }
        }
      })
  },

  tapHelp: function (e) {
    if (e.target.id == 'help') {
      this.hideHelp();
    }
  },
  showHelp: function (e) {
    // console.log(e)
    var that = this;
    that.setData({
      'header.help_status': true
    });
  },
  showHelpClass: function (e) {
    // console.log(e)
    var that = this;
    that.setData({
      'header.help_class_status': true
    });
  },
  hideHelp: function (e) {
    this.setData({
      'header.help_status': false,
      'header.help_class_status': false
    });
  },
});