// pages/school/cert.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    total:0,
    opacity:'.3',//字体透明度
    color2:'#081EF',//字体颜色
    number:6,//水印数量
    deg:'-45',//旋转角度
    isLoading: true,
    userInfo:null,
    tipsWindowsFlag:true,
    modalName:null,
    neirong:null,
    hasWeappMethod: ['http://cet.neea.edu.cn/cet/'],
    cardColor: ['red', 'orange', 'yellow', 'olive', 'green', 'blue', 'purple', 'mauve', 'pink'],
    orderby: '报价文件-漏损控制',
    orderbyList: [{
      title: '漏损控制',
      value: '报价文件-漏损控制'
    }, {
      title: '智慧供水',
      value: '报价文件-智慧供水'
    }, {
      title: '智慧水务',
      value: '报价文件-智慧水务'
    }],
    queryParam: {
      pageIndex: "1",
      pageSize: "1000",
      name: "报价文件-漏损控制"
    },
    certList: [],
  },
  keywordInput: function (e) {
    this.setData({
      'queryParam.title': e.detail.value
    });
  },
  searchBooks:function (params) {
    this.getHotBooksData(this.data.orderby)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onPageScroll(e) {
    this.setData({
      scrollTop: e.scrollTop
    })
  },
  inital: function (orderby) {
    this.setData({
     'queryParam.name': orderby,
     userInfo: app.globalData.userInfo
       });
  },
  orderbyChanged: function (e) {
    const orderby = this.data.orderbyList[e.currentTarget.dataset.index].value;
    this.setData({ orderby: orderby});
    this.getHotBooksData(orderby);
  },
  getHotBooksData: function (orderby) {
    this.inital(orderby)
    app.formPost('/api/wx/student/book/pageList', this.data.queryParam)
    .then(res => {
      if (res.code == 1) {
        this.setData({
          certList: res.response.list,
          total:res.response.total,
          isLoading: false
        })
      } else {
        wx.showModal({
          title: res.message
        })
      }
    }).catch(e => {
      wx.showModal({
        title: e
      })
    })
  },

  onLoad: function (options) {
    const orderby = options.orderby ? options.orderby : '报价文件-漏损控制';
    this.setData({
      userInfo: app.globalData.userInfo
    })
    this.inital(orderby);
    this.getHotBooksData(orderby);
    this.initall();
  },
  initall: function () {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  navActivity(e) {
    const id = e.currentTarget.dataset.id;
    wx.showModal({
      title: "积分信息提示",
      content:
        "点击查看需要消耗您的积分，" +
        "请确认后点击【我已知晓】。",
      confirmText: "我已知晓",
      confirmColor: "#4B6DE9",
      cancelText: "我拒绝",
      success: result => {
        if (!result.confirm) {
         wx.navigateBack({
           delta: 0,
         })
        } else {
          wx.navigateTo({
            url: '../process/process?id=' + id,
        });
        }
      }
    });
  },
  goToHook(e) {
    var that = this;
    const url = e.currentTarget.dataset.target
    console.log(url)
    that.setData({
      modalName: "Modal",
      neirong: url
    });
    wx.setClipboardData({
      data: url,
      success(res) {
        wx.getClipboardData({
          success(res) {
            console.log(res.data) // data
            wx.showToast({
              title: '复制成功,粘贴到浏览器访问',
              icon: 'none'
            })
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    // setTimeout(function () {
    //   that.setData({
    //     isLoading: false
    //   });
    // }, 400);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '“在线考试” - 产品报价',
      path: '/packageG/cert/cert'
    }
  },
})