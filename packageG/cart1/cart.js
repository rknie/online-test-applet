const app = getApp();
Component({
  data: {
    opacity:'.3',//字体透明度
    color2:'#081EF',//字体颜色
    number:6,//水印数量
    deg:'-45',//旋转角度
    showLogin: false,
    mode:'circle',// circle 圆形 square 方形
    bottom:100,//距离顶部距离 rpx
    top:50,//距离顶部多少距离显示 px
    bgColor:'blue',//背景色
    color:'#fff',//文字图标颜色
    color1:'#9a9a9a',//文字图标颜色
    icon:'triangleupfill',//图标
    right:'24',//距离右侧距离 rpx
    scrollTop:0,
    isLoading: true,
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    queryParam: {
      pageIndex: "1",
      pageSize: "100",
      category:"考试复习",
      dept:""
    },
    bookList:[],
    neirong: null,
    userInfo:null,
  },
  pageLifetimes: {
    show() {
      if (typeof this.getTabBar === 'function' && this.getTabBar()) {
        this.getTabBar().setData({
          selected: 2
        })
      }
    }
  },
   methods: {
        // 跳转到资讯详情
        navActivity(e) {
          const id = e.currentTarget.dataset.id; 
          wx.showModal({
            title: "积分信息提示",
            content:
              "点击查看需要消耗您的积分，" +
              "请确认后点击【我已知晓】。",
            confirmText: "我已知晓",
            confirmColor: "#4B6DE9",
            cancelText: "我拒绝",
            success: result => {
              if (!result.confirm) {
               wx.navigateBack({
                 delta: 0,
               })
              } else {
                wx.navigateTo({
                  url: '../process1/process?id=' + id,
              });
              }
            }
          });

             
        },
        onPullDownRefresh() {
          this.setData({
            isLoading: true
          });
          if (!this.loading) {
            this.search()
          }
        },
            //页面滚动执行方式
    onPageScroll(e) {
      this.setData({
        scrollTop: e.scrollTop
      })
    },
    bindSearchInput: function (e) {
      this.setData({
        'queryParam.dept': e.detail.value
      });
    },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var category = options.category
    this.setData({
      'queryParam.category': category,
      isLoading: true,
      userInfo:app.globalData.userInfo
    });
   this.search()
  },
  search: function () {
    app.formPost('/api/wx/student/wenDang/pageList', this.data.queryParam)
    .then(res => {
      if (res.code == 1) {
        this.setData({
          bookList: res.response.list,
          isLoading: false,
        })
      } else {
        wx.showModal({
          title: res.message
        })
      }
    }).catch(e => {
      wx.showModal({
        title: e
      })
    })
  }
    }
})