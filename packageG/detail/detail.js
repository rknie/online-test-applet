// pages/books/detail.js
var app = getApp();
Page({
  data: {
    height:'200',
    color:'#0081ef',
    opacity: '.3', //字体透明度
    color2: '#081EF', //字体颜色
    number: 6, //水印数量
    deg: '-45', //旋转角度
    isLoading: '加载中',
    id:"",
    list: [],
  },
  onLoad: function (e) {
    var id = e.id;
    let _this = this
    app.formPost('/api/wx/student/wenda/select/' + id, null)
    .then(res => {
      if (res.code === 1) {
        _this.setData({
          isLoading: "",
          list:res.response       
        });
        wx.showToast({
          title: '已扣除1积分！',
          icon: 'none',
          duration: 2000
          })   
      }else {
        _this.setData({
          isLoading: ""   
        });
        wx.showModal({
          title: res.message
        })
      };
    })
  },
})