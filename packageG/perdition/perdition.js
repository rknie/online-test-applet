
var app = getApp();
Page({
  data: {
    opacity:'.3',//字体透明度
    color2:'#081EF',//字体颜色
    number:6,//水印数量
    deg:'-45',//旋转角度
    mode: 'circle', // circle 圆形 square 方形
    bottom: 100, //距离顶部距离 rpx
    top: 50, //距离顶部多少距离显示 px
    bgColor: 'blue', //背景色
    color: '#fff', //文字图标颜色
    icon: 'triangleupfill', //图标
    right: '24', //距离右侧距离 rpx
    scrollTop: 0,
    isLoading: true,
    userInfo: null,
    modalName: null,
    id:"",
    list: [],
  },
  onLoad: function (e) {
    var id = e.id;
    let _this = this
    app.formPost('/api/wx/student/material/select/' + id, null)
    .then(res => {
      if (res.code === 1) {
        _this.setData({
          list:res.response,
          isLoading: false     
        });
        wx.showToast({
          title: '已扣除'+res.response.integral+'积分！',
          icon: 'none',
          duration: 2000
          })   
      }else {
        _this.setData({
          isLoading: true   
        });
        wx.showModal({
          title: res.message
        })
      };
    })
  },
  copyMessage:function(e){
      var that = this;
      const name = that.data.list.name
      const brand = that.data.list.brand
      const model = that.data.list.model
      const price = that.data.list.price*that.data.list.coefficient*0.001
      const totalprice = price*that.data.list.coefficient*0.001
        wx.setClipboardData({
          data: "物料名称：" + name + "    品牌：" + brand + "    型号：" + model+"    分公司成本价：" + price+ "    建议面价：" + totalprice,
          success(res) {
            wx.getClipboardData({
              success(res) {
                console.log(res.data) // data
                wx.showToast({
                  title: '复制成功！',
                  icon: 'none'
                })
              }
            })
          }
        })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '“在线考试” - 物料产品',
      path: '/packageG/perdition/perdition'
    }
  },
});