const app = getApp();
Page({
  data: {
    title: '',
    userInfo:null,
    itemList:null,
    ergong:[],
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar
  },
  clickImg(event) {
    console.log('event', event);
    const imageUrl = event.currentTarget.dataset.url

    wx.previewImage({
      current: imageUrl,
      urls: [imageUrl]
    })
  },
  toIndex: function () {
    wx.navigateTo({
      url: '/pages/project/project',
    })
  },

  onLoad: function () {
    this.setData({
      userInfo:app.globalData.userInfo,
      ergong:app.globalData.ergong,
      itemList:app.globalData.itemList
    })
  },
});