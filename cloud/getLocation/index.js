// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const rp = require('request-promise')
// 云函数入口函数
exports.main = async (event, context) => {

  var options = {
    uri: `https://apis.map.qq.com/ws/geocoder/v1/`,
    qs: {
      location: `${event.lat},${event.lng}`,//传入经纬度
      key: '2ZWBZ-WIEKD-QSU43-P52YG-Y3L7F-X5FM7'
    },
    json: true 
  };
  return await rp(options)
}