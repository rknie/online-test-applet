Component({
            props: {
              // 距离底部的距离
              bottom: {
                type: [Number, String],
                default: 300
              },

              // 距离右边的距离
              right: {
                type: [Number, String],
                default: 75
              },

              // 首页地址
              indexPath: {
                type: String,
                default: '/pages/index/index'
              }
            },


            methods: {
              // 跳转回首页
              navIndex: function navIndex() {
                // 通过判断当前页面的页面栈信息，是否有上一页进行返回，如果没有则跳转到首页
                var pages = getCurrentPages();
                if (pages && pages.length > 0) {
                  var _indexPath = this.indexPath || '/pages/index/index';
                  var firstPage = pages[0];
                  if (!firstPage.route || firstPage.route != _indexPath.substring(1, _indexPath.length)) {
                    uni.reLaunch({
                      url: _indexPath
                    });

                  } else {
                    uni.navigateBack({
                      delta: 1
                    });

                  }
                } else {
                  uni.reLaunch({
                    url: indexPath
                  });

                }
              }
            }

          }) 