var app = getApp();
Page({
  data: {
    version:app.globalData.version
  },
  subMsg() {
    wx.requestSubscribeMessage({
      tmplIds: ['Q5B4N5l0cmyoNYC7siPyF7rv-xN6aT8FV3jWqUCRQ2Y', 'MPdYYz2g1BlAWRyg9bUOiOZwS6Q0l_1Ag48OyM6F1bs', 'Ys-DSceVKE9EdxlkZgUceNNbikobYAntZ_weRNvG0qE'],
      success(res) {
        console.log("授权成功！")
        wx.showToast({
          title: '订阅成功！',
          icon: 'success',
          duration: 2000 //持续的时间
        })
        wx.vibrateShort({
          type: 'heavy'
        })
      },
      fail(res) {
        wx.showToast({
          title: '订阅失败！！！',
          icon: 'error',
          duration: 2000 //持续的时间
        })
      }
    })
  },
  goSign() {
    var that = this;

    that.setData({
      isLoading: true
    });
    setTimeout(function () {
      that.setData({
        isLoading: false
      });
    }, 3000);
    wx.switchTab({
      url: '/pages/NewWelcome/index',
    })
  },
  gotoIndex() {
    wx.setStorageSync('isLogin', true);
    wx.setStorageSync('isFirst', true);
    app.globalData.isLogin = true;
    
      wx.vibrateShort({ type: 'medium' })
      wx.switchTab({
        url: '../../pages/index/index',
      })
  },

  onLoad: function () {
    this.setData({
      year: new Date().getFullYear()
    });
  },
  onShow: function () {

  },
  onReady: function () {
    
  },
});