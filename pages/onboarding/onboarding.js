const app = getApp()
Page({
  data: {
    step: 1,
    version:app.globalData.version
  },
  onLoad() {
    //每一位用户，都需要在onboarding的页面上触发新增用户到USERS表
      wx.setStorageSync('isFirst', true)
  },
  next() {
    const { step } = this.data
    if (Number(step) === 6) {
      wx.redirectTo({
        url: '/pages/NewWelcome/loadPage',
      })
      // wx.setStorageSync('isOnboarding', 'v3.5.0')
    } else {
      wx.vibrateShort()
      this.setData({
        step: step + 1,
      })
    }
  },
})
