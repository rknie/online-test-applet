// const ccminiPageHelper = require('../../helper/ccmini_page_helper.js');
// const PassportBiz = require('../../biz/passport_biz.js'); 
const app = getApp()
let videoAd = null
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
    StatusBar: app.globalData.StatusBar,
		CustomBar: app.globalData.CustomBar,
		newUser:null
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: async function (options) {
// 在页面onLoad回调事件中创建激励视频广告实例
if (wx.createRewardedVideoAd) {
  videoAd = wx.createRewardedVideoAd({
    adUnitId: 'adunit-95e62e7e8f7225c1'
  })
  videoAd.onLoad(() => {})
  videoAd.onError((err) => {})
  videoAd.onClose((res) => {})
}
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},
	url:function(){
		wx.navigateTo({
			url: '/pages/introduce1/index',
		})
	},

	url1:function(){
		wx.navigateTo({
			url: '/pages/my/memberCode/memberCode',
		})
	},
	url2:function(){
		let that = this;
		wx.showModal({
			title: '提示',
			content: '点击确认，观看视频后，可获取10积分',
			success (res) {
				if (res.confirm) {
					if (videoAd) {
						videoAd.show().catch(() => {
							// 失败重试
							videoAd.load()
								.then(() => videoAd.show())
								.catch(err => {
									console.log('激励视频 广告显示失败')
								})
						})
					}
					videoAd.onClose(res => {
						// 用户点击了【关闭广告】按钮
						if (res && res.isEnded) {
							// 正常播放结束，可以下发游戏奖励
								app.formPost('/api/wx/student/user/select/'+app.globalData.userInfo.id, null)
								.then(res => {
									if (res.code == 1&&app.globalData.userInfo.userName!="student") {
										that.setData({
											newUser:res.response,
											'newUser.integral':res.response.integral+10,
										})
										app.formPost('/api/wx/student/user/updateIntegral', that.data.newUser).then(
											res1 => {
												if (res1.code == 1) {
													wx.showModal({
														title: '新增10积分成功！请退出重进小程序，刷新积分。',
													})
												}
											})
									} else {
										wx.showModal({
											title: '新增积分失败！',
										})
									}
								})
						} else {
							
						}
				})				
				} else if (res.cancel) {
					console.log('用户点击取消')
				}
			}
		})
		
	},
	url3:function(){
		wx.navigateTo({
			url: '/pages/jifen/jifen',
		})
	},
	url4:function(){
		wx.showToast({
			title: '功能开发中！',
		})
	},
	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},


	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})