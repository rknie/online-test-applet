// pages/school/cert.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mode: 'circle', // circle 圆形 square 方形
    bottom: 100, //距离顶部距离 rpx
    top: 50, //距离顶部多少距离显示 px
    bgColor: 'blue', //背景色
    color: '#fff', //文字图标颜色
    icon: 'triangleupfill', //图标
    right: '24', //距离右侧距离 rpx
    scrollTop: 0,
    isLoading: true,
    userInfo: null,
    modalName: null,
    neirong: null,
    cardColor: ['red', 'orange', 'yellow', 'olive', 'green', 'blue', 'purple', 'mauve', 'pink'],
    certList: [],
    certList1: [],
    certList2: [],
    certList3: [],
    queryParam: {
      pageIndex: "1",
      pageSize: "1000",
      name: "竞争对手-和达"
    },
    queryParam1: {
      pageIndex: "1",
      pageSize: "1000",
      name: "竞争对手-熊猫"
    },
    queryParam2: {
      pageIndex: "1",
      pageSize: "1000",
      name: "竞争对手-其他"
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  //页面滚动执行方式
  onPageScroll(e) {
    this.setData({
      scrollTop: e.scrollTop
    })
  },
  onLoad: function (options) {
    app.formPost('/api/wx/student/book/pageList', this.data.queryParam)
      .then(res => {
        if (res.code == 1) {
          this.setData({
            certList: res.response.list,
            userInfo: app.globalData.userInfo
          })
        } else {
          wx.showModal({
            title: res.message
          })
        }
      }).catch(e => {
        wx.showModal({
          title: e
        })
      })
    app.formPost('/api/wx/student/book/pageList', this.data.queryParam1)
      .then(res => {
        if (res.code == 1) {
          this.setData({
            certList1: res.response.list,
          })
        } else {
          wx.showModal({
            title: res.message
          })
        }
      }).catch(e => {
        wx.showModal({
          title: e
        })
      })
    app.formPost('/api/wx/student/book/pageList', this.data.queryParam2)
      .then(res => {
        if (res.code == 1) {
          this.setData({
            certList2: res.response.list,
          })
        } else {
          wx.showModal({
            title: res.message
          })
        }
      }).catch(e => {
        wx.showModal({
          title: e
        })
      })
    this.setData({
      userInfo: app.globalData.userInfo
    })
    this.inital();
  },
  inital: function () {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  navActivity(e) {
    const id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../process/process?id=' + id,
  });
  },
  goToHook(e) {
    var that = this;
    const url = e.currentTarget.dataset.target
    console.log(url)
    that.setData({
      modalName: "Modal",
      neirong: url
    });
    wx.setClipboardData({
      data: url,
      success(res) {
        wx.getClipboardData({
          success(res) {
            console.log(res.data) // data
            wx.showToast({
              title: '复制成功,粘贴到浏览器访问',
              icon: 'none'
            })
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    setTimeout(function () {
      that.setData({
        isLoading: false
      });
    }, 400);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '“在线考试” - 竞争对手',
      path: 'pages/book/book'
    }
  },
})