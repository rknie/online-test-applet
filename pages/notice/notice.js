
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    opacity: '.3', //字体透明度
    color2: '#081EF', //字体颜色
    number: 6, //水印数量
    deg: '-45', //旋转角度
    notice: [],
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    that.setData({
      notice:app.globalData.inportance_notice
    })
  },
  // back() {
  //   wx.redirectTo({
  //     url: '../index/index',
  //   });
  // },

})