const app = getApp();
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    showLogin: false,
    mode:'circle',// circle 圆形 square 方形
    bottom:100,//距离顶部距离 rpx
    top:50,//距离顶部多少距离显示 px
    bgColor:'blue',//背景色
    color:'#fff',//文字图标颜色
    color1:'#9a9a9a',//文字图标颜色
    icon:'triangleupfill',//图标
    right:'24',//距离右侧距离 rpx
    scrollTop:0,
    shuLiang:null,
    rankList:[],
    loading: true,
    index:0
  },
    //页面滚动执行方式
    onPageScroll(e) {
      this.setData({
        scrollTop: e.scrollTop
      })
    },
      onLoad (options) {
        var examPaperId = options.examPaperId
        app.formPost('/api/wx/student/exampaper/answer/rankListOne'+'/'+examPaperId, null).then(res => {
          console.log(res)
        this.setData({
          rankList:res.response,
          loading:false
           })
        })    
    },
});