// pages/user/info/index.js
const app = getApp()
Page({
  data: {
    userInfo: null,
    oldPassword:null,
    newPassword: null,
    newPassword1: null,
    spinShow: false,
    user: {
      id: 0,
      age: 0,
      realName: null,
      phone: null,
      password: null
    },
  },
  onLoad: function () {
    this.loadUserInfo()
  },
  loadUserInfo() {
    let _this = this
    _this.setData({
      spinShow: true
    });
    app.formPost('/api/wx/student/user/current', null).then(res => {
      if (res.code == 1) {
        _this.setData({
          userInfo: res.response,
          'user.id': res.response.id,
          'user.age': res.response.age,
          'user.realName': res.response.realName,
          'user.password': this.data.newPassword,
          'user.phone': res.response.phone
        });
      }
      _this.setData({
        spinShow: false
      });
    }).catch(e => {
      _this.setData({
        spinShow: false
      });
      app.message(e, 'error')
    })
  },
  formSubmit: function (e) {
    let _this = this
    console.log(e)
    if (e.detail.value.oldPassword==null||e.detail.value.newPassword==null||e.detail.value.newPassword1==null) {
      wx.showModal({
        title: '提示',
        content: '请输入密码！'
      })
      return
    };
    if (e.detail.value.oldPassword.length < 6) {
      wx.showModal({
        title: '提示',
        content: '原密码输入错误，请重新输入！'
      })
      return
    };
    if (e.detail.value.oldPassword == e.detail.value.newPassword||e.detail.value.oldPassword == e.detail.value.newPassword1) {
      wx.showModal({
        title: '提示',
        content: '原密码与新密码一致，请重新输入！'
      })
      return
    };
    if (e.detail.value.newPassword.length < 6 || e.detail.value.newPassword1.length < 6) {
      wx.showModal({
        title: '提示',
        content: '密码长度不够6位，请重新输入！'
      })
      return
    };
    if (e.detail.value.newPassword != e.detail.value.newPassword1) {
      wx.showModal({
        title: '提示',
        content: '两次输入密码不一致，请重新输入！'
      })
      return
    };
    if (e.detail.value.newPassword == e.detail.value.newPassword1 && e.detail.value.newPassword.length >= 6) {
      _this.setData({
        'user.password': e.detail.value.newPassword1
      });
      wx.showLoading({
        title: '提交中',
        mask: true
      })
      app.formPost('/api/wx/student/user/update', _this.data.user)
        .then(res => {
          if (res.code == 1) {
            wx.reLaunch({
              url: '/pages/my/index/index',
            });
          } else {
            wx.showModal({
              title: '提示',
              content: res.message
            })
          }
          wx.hideLoading()
        }).catch(e => {
          wx.showModal({
            title: '提示',
            content: e
          })
          wx.hideLoading()
        })
    }
  }
})