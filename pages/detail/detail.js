// pages/books/detail.js
var app = getApp();
Page({
  data: {
    isLoading: '加载中',
    id:"",
    list: [],
  },
  onLoad: function (e) {
    var id = e.id;
    let _this = this
    wx.showLoading({ title: "等我加载一下~" });
    app.formPost('/api/wx/student/wenda/select/' + id, null)
    .then(res => {
      if (res.code === 1) {
        _this.setData({
          isLoading: "",
          list:res.response       
        });
        
          wx.hideLoading()
         
      };
    })
  },
})