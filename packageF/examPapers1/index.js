//index.js
//获取应用实例
const app = getApp()
const jinrishici = require('../../utils/jinrishici.js')
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    spinShow: false,
    shici: '长风破浪会有时，直挂云帆济沧海。',
    fixedPaper: [],
    pushPaper: [],
    timeLimitPaper: [],
    taskList: [],
    msgList: []
  },
  onLoad: function () {
    this.setData({
      spinShow: true
    });
    this.indexLoad()
    app.formPost('/api/wx/student/user/current', null).then(res => {
      if (res.code == 1) {
        app.globalData.userInfo = res.response
      }
    }).catch(e => {
      app.message(e, 'error')
    })
  },

  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '在线考试exam',
      path: '/page/index/index'
    }
  },
  onShareTimeline: function (t) {
    return {
      title: "在线考试exam",
      query: "",
      imageUrl: ""
    }
  },
  onPullDownRefresh() {
    this.setData({
      spinShow: true
    });
    if (!this.loading) {
      this.indexLoad()
    }
  },
  indexLoad: function () {
    let _this = this
    app.formPost('/api/wx/student/dashboard/index', null).then(res => {
      _this.setData({
        spinShow: false
      });
      wx.stopPullDownRefresh()
      if (res.code === 1) {
        _this.setData({
          fixedPaper: res.response.fixedPaper,
          timeLimitPaper: res.response.timeLimitPaper,
          pushPaper: res.response.pushPaper,
          msgList: res.response.msgList
        });
      }
    }).catch(e => {
      _this.setData({
        spinShow: false
      });
    })

    app.formPost('/api/wx/student/dashboard/task', null).then(res => {
      _this.setData({
        spinShow: false
      });
      wx.stopPullDownRefresh()
      if (res.code === 1) {
        _this.setData({
          taskList: res.response
        });
      }
    }).catch(e => {
      _this.setData({
        spinShow: false
      });
    })
  },


})