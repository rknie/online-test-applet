var t = getApp();

Page({
    data: {
        NavBarHeight: t.globalData.NavBarHeight,
        StatusBar: t.globalData.StatusBar,
        CustomBar: t.globalData.CustomBar,
        Custom: t.globalData.Custom,
        question: "",
        result: [ {
            title: "请输入关键字",
            content: "长按可复制答案哦"
        } ],
        contents: ""
    },
    onChange: function(t) {
        this.setData({
            question: t.detail
        });
    },
    formReset: function(t) {
        console.log("form发生了reset事件，携带数据为：", t.detail.value), this.setData({
            content: ""
        });
    },
    copyanswer: function(t) {
        var a = t.currentTarget.dataset.value, o = this.data.result[a].content;
        console.log("答案已复制！"), wx.setClipboardData({
            data: String(o)
        });
    },
    submit: function(t) {
        var a = this, o = t.detail.value.content;
        if (o && "" != o.trim()) {
            var n = o;
            wx.showLoading({
                title: "功能开发中！"
            })
        } else wx.showToast({
            title: "请输入内容",
            icon: "none",
            mask: !0
        });
    },
    onLoad: function(t) {
        // getApp().SetColor(this);
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {},
    onReachBottom: function() {},
    onShareAppMessage: function() {}
});