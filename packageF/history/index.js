// pages/history/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    loading: true,
    total: 0,
    jigeList:[],
    jige: 0,
    userScore: 0,
    paperScore: 0,
    historyList: [],
    spinShow: false
  },
  onLoad() {
    let _this = this;
    _this.setData({
      spinShow: true
    });

    app.formPost('/api/wx/student/exampaper/answer/history', null).then(res => {
      console.log(res)
      var total = res.response.length;
      console.log(total)
      var jige = 0;
      var jigeList=[];
      for (var i = 0; i <= res.response.length - 1; i++) {
        if (res.response[i].userScore >= res.response[i].paperScore * 0.6) {
          jige++;
          jigeList.push(res.response[i]);
          console.log(jige)
        }
      }
      _this.setData({
        spinShow: false,
        historyList: res.response,
        jige: jige,
        jigeList:jigeList,
        total: res.response.length,
        loading: false
      })
    })
  }
})